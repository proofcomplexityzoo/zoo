#!/usr/bin/env python3

import sys

ps1 = sys.argv[1]
ps2 = sys.argv[2]+"_"

import zoo

for f in zoo.formulas:
    if f in zoo.reach[ps1] and ps2 in zoo.reach[f]:
        print(f)
